from json import JSONEncoder

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

import json
import pandas
import requests
import shutil
from .metadata import __copyright__, __license__, __version__


class SimpleRequest(object):
    pass


class EvaluationRequest(SimpleRequest):
    def __init__(self, identifier=None, arguments=None):
        self.id = identifier
        if arguments is None:
            self.arguments = {}
        else:
            self.arguments = arguments


class SimpleResponse(object):
    def __init__(self, message=None, skip_check=False):
        self.message = message
        self.skip = skip_check

    def ensure_success(self):
        if self.skip:
            return self
        else:
            if hasattr(self, "message") and self.message is not None:
                raise Exception(self.message)
            return self


class EvaluationResponse(SimpleResponse):
    def __init__(self, message=None, identifier=None, result=None, skip_check=False):
        super(EvaluationResponse, self).__init__(message, skip_check)
        self.id = identifier
        if result is None:
            self.result = {}
        else:
            self.result = result


class ModelResponse(SimpleResponse):
    def __init__(self, message=None, identifier=None, mining_function=None, summary=None, properties=None, schema=None):
        super(ModelResponse, self).__init__(message)
        self.id = identifier
        self.miningFunction = mining_function
        self.summary = summary
        if properties is None:
            self.properties = {}
        else:
            self.properties = properties
        if schema is None:
            self.schema = {}
        else:
            self.schema = schema


class RequestEncoder(JSONEncoder):
    def default(self, request):
        if isinstance(request, SimpleRequest):
            return request.__dict__
        else:
            return JSONEncoder.default(self, request)


def _merge_dicts(user_dict, **system_dict):
    if user_dict is None:
        return system_dict
    for key in system_dict:
        system_value = system_dict[key]
        if key in user_dict:
            user_value = user_dict[key]
            if isinstance(user_value, dict) and isinstance(system_value, dict):
                user_value.update(system_value)
            elif user_value == system_value:
                pass
            else:
                raise Exception()
        else:
            user_dict[key] = system_value
    return user_dict


class Openscoring(object):
    def __init__(self, base_url="http://localhost:8080/openscoring"):
        self.base_url = base_url

    def _model_url(self, identifier):
        return self.base_url + "/" + identifier

    def _check_response(self, response, skip=False):
        if skip:
            return response
        else:
            try:
                service = response.headers["Service"]
                if service.startswith("Openscoring/1.4") is False:
                    raise ValueError(service)
            except (KeyError, ValueError):
                raise ValueError(
                    "The web server at " + self.base_url + " did not identify itself as Openscoring/1.4 service")
            return response

    def evaluate(self, identifier, payload=None, **kwargs):
        if payload is None:
            payload = {}
        kwargs = _merge_dicts(kwargs, data=json.dumps(payload, cls=RequestEncoder), json=None,
                              headers={"content-type": "application/json"})
        response = self._check_response(requests.post(self._model_url(identifier), **kwargs), skip=True)
        evaluation_response = EvaluationResponse(json.loads(response.text), skip_check=True)
        evaluation_response.ensure_success()
        if isinstance(payload, EvaluationRequest):
            return evaluation_response
        else:
            return evaluation_response.message

    def evaluate_csv(self, identifier, df, **kwargs):  # NEEDS UPDATING
        csv = df.to_csv(None, sep="\t", header=True, index=False, encoding="UTF-8")
        kwargs = _merge_dicts(kwargs, data=csv, json=None, headers={"content-type": "text/plain"},
                              params={"delimiterChar": "\t", "quoteChar": "\""}, stream=False)
        response = self._check_response(requests.post(self._model_url(identifier) + "/csv", **kwargs), skip=True)
        try:
            if "content-encoding" in response.headers:
                response.raw.decode_content = True
            if ("content-type" in response.headers) and (response.headers["content-type"] == "application/json"):
                simple_response = SimpleResponse(**json.loads(response.text))
                return simple_response.ensure_success()
            return pandas.read_csv(StringIO(response.text), sep="\t", encoding="UTF-8")
        finally:
            response.close()

    def evaluate_csv_file(self, identifier, infile, outfile, **kwargs):  # NEEDS UPDATING
        with open(infile, "rb") as instream:
            kwargs = _merge_dicts(kwargs, data=instream, json=None, headers={"content-type": "text/plain"}, stream=True)
            response = self._check_response(requests.post(self._model_url(identifier), **kwargs), skip=True)
            try:
                if "content-encoding" in response.headers:
                    response.raw.decode_content = True
                if ("content-type" in response.headers) and (response.headers["content-type"] == "application/json"):
                    simple_response = SimpleResponse(json.loads(response.text), skip_check=True)
                    return simple_response.ensure_success()
                with open(outfile, "wb") as outstream:
                    shutil.copyfileobj(response.raw, outstream, 1024)
            finally:
                response.close()
