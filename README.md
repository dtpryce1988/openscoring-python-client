Openscoring-Python
==================
Python client library for the Openscoring REST web service.

# Prerequisites #

* Python 2.7, 3.4 or newer.

# Installation #
WARNING: this is not the publicly available version of this library.
We have diverged from that code base to be able to use our own REST API.
Install with this command:

``` pip install -i https://pypi.wandera.net/simple/ openscoring ```

# Usage #

Creating an `Openscoring` object:
```python
from openscoring import Openscoring

os = Openscoring("http://localhost:8080/openscoring")
```

Deploying a PMML document `DecisionTreeIris.pmml` as an `Iris` model
is not simple due to our DropWizard service requirements.
For now #askdatascience

Evaluating the `Iris` model with a data record:
```python
arguments = {
	"Sepal_Length" : 5.1,
	"Sepal_Width" : 3.5,
	"Petal_Length" : 1.4,
	"Petal_Width" : 0.2
}

result = os.evaluate("Iris", arguments)
print(result)
```

<!-- The same, but wrapping the data record into an `EvaluationRequest` object for request identification purposes: -->
<!-- ```python -->
<!-- from openscoring import EvaluationRequest -->

<!-- evaluationRequest = EvaluationRequest("record-001", arguments) -->

<!-- evaluationResponse = os.evaluate("Iris", evaluationRequest) -->
<!-- print(evaluationResponse.result) -->
<!-- ``` -->

<!-- Evaluating the `Iris` model with data records from the `Iris.csv` CSV file, storing the results to the `Iris-results` CSV file: -->
<!-- ```python -->
<!-- os.evaluateCsvFile("Iris", "Iris.csv", "Iris-results.csv") -->
<!-- ``` -->


# De-installation #

Uninstall:
```
pip uninstall openscoring
```

# License #

Openscoring-Python is dual-licensed under the [GNU Affero General Public License (AGPL) version 3.0](http://www.gnu.org/licenses/agpl-3.0.html), and a commercial license.
