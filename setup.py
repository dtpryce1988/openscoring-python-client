from distutils.core import setup

exec(open("openscoring/metadata.py").read())

setup(
	name = "openscoring",
	version = __version__,
	description = "Python client library for the Wandera Openscoring REST web service based on "
				  "https://github.com/openscoring/openscoring)",
	license = __license__,
	packages = [
		"openscoring"
	],
	install_requires = [
		"requests",
		"pandas"
	]
)
